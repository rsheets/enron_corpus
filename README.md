# Enron Spreadsheet Corpus

As described [here](http://www.felienne.com/archives/3634) by [Felienne Hermans](http://www.felienne.com/).

This repository is simply the files from the [figshare archive](https://figshare.com/articles/Enron_Spreadsheets_and_Emails/1221767) unpacked and an index added so we can easily access them individually.

The data set were released under a [CC-BY](https://creativecommons.org/licenses/by/4.0/) licence.  After they were released by subpoena.
